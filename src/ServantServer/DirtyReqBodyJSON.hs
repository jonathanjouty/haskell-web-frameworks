{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-
This module provides a way to handle JSON request bodies with "dirty"
Content-Type: either text/plain, or application/octet-stream (or nothing, as
the latter is the default).
-}
module ServantServer.DirtyReqBodyJSON
( DirtyReqBodyJSON
, DirtyJSONList
) where

import Data.Aeson (ToJSON, FromJSON)
import Data.Typeable (Proxy, Proxy(..), Typeable)
import Servant.API

data JSONAsOctetStream deriving Typeable
data JSONAsPlainText   deriving Typeable

instance Accept JSONAsOctetStream where
  contentType _ = contentType (Proxy :: Proxy OctetStream)
instance Accept JSONAsPlainText where
  contentType _ = contentType (Proxy :: Proxy PlainText)

instance ToJSON a => MimeRender JSONAsOctetStream a where
  mimeRender _ = mimeRender (Proxy :: Proxy JSON)
instance ToJSON a => MimeRender JSONAsPlainText a where
  mimeRender _ = mimeRender (Proxy :: Proxy JSON)

instance FromJSON a => MimeUnrender JSONAsPlainText a where
  mimeUnrender _ = mimeUnrender (Proxy :: Proxy JSON)
instance FromJSON a => MimeUnrender JSONAsOctetStream a where
  mimeUnrender _ = mimeUnrender (Proxy :: Proxy JSON)

type DirtyJSONList = '[JSON, JSONAsPlainText, JSONAsOctetStream]

type DirtyReqBodyJSON a = ReqBody DirtyJSONList a
