module Main where

import Network.Wai.Middleware.RequestLogger
import Web.Scotty

import Template

main :: IO ()
main = scotty 5050 $ do

  middleware logStdoutDev

  get "/" hello

  post "/template/:template_id" $ do
    _tid <- (param "template_id" :: ActionM Int)
    d <- (jsonData :: ActionM Template)
    json d

hello :: ActionM ()
hello = do
  html "Hello World!"
