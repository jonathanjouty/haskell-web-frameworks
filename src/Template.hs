module Template where

import Data.Text
import GHC.Generics
import qualified Data.Aeson as Aeson

data Template = Template {
    id :: Int
  , name :: Text
  , matches :: Maybe [Match]
  } deriving (Show, Generic)

instance Aeson.ToJSON Template
instance Aeson.FromJSON Template

data Match = Match {
    matchText :: Text
  , matchPage :: Maybe Int
  } deriving (Show, Generic)

instance Aeson.ToJSON Match
instance Aeson.FromJSON Match
