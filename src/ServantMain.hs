module Main where

import Data.Proxy
import Data.Text (Text)
import Network.Wai.Handler.Warp
import Network.Wai.Middleware.RequestLogger
import Servant.API
import Servant.Server

import ServantServer.DirtyReqBodyJSON
import Template

type TemplateAPI =

  Get '[PlainText] Text :<|>

  "template" :> (
    Capture "template_id" Int
    :> ReqBody '[JSON] Template
    :> Post '[JSON] Template
    :<|>

    "dirty"
    :> Capture "template_id" Int
    :> DirtyReqBodyJSON Template
    :> Post '[JSON] Template
  )

templateServer :: Server TemplateAPI
templateServer =
       handleHello
  :<|> handleTemplate
  :<|> handleTemplate

handleHello :: Handler Text
handleHello = return "Hello World!"

handleTemplate :: Int -> Template -> Handler Template
handleTemplate _ t = return t

main :: IO ()
main = run 5051
  (logStdoutDev $ serve (Proxy :: Proxy TemplateAPI) templateServer)
