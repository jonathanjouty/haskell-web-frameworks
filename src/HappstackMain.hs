module Main where

import Control.Monad
import Data.Aeson
import Happstack.Server
import qualified Data.ByteString.Lazy.Char8 as BLC

import Template

main :: IO ()
main = simpleHTTP nullConf $ msum
  [ method GET >> ok "Hello world!"
  , dir "template" $ path $ \(_tid :: Int) -> do
      decodeBody (defaultBodyPolicy "tmp/" 4096 4096 4096)
      b <- body $ look ""
      let md = decode $ BLC.pack b :: Maybe Template
      case md of
        Just d -> ok $ BLC.unpack $ encode d
        Nothing -> badRequest "Invalid template JSON"
  ]
